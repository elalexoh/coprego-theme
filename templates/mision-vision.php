<?php
/* Template Name: Mision y Vision Template */

$img_main_bg = get_field('img_main_bg', 'option');
$img_left_bg = get_field('img_left_bg', 'option');
$txt_main_title1 = get_field('txt_main_title1', 'option');
$txt_mision_title = get_field('txt_mision_title', 'option');
$txt_mision_p = get_field('txt_mision_p', 'option');
$txt_vision_title = get_field('txt_vision_title', 'option');
$txt_vision_p = get_field('txt_vision_p', 'option');

?>
<div class="block-mision-vision">
    <div class="container-fluid h-100 wrapper-content">
        <div class="row justify-content-center align-items-center dp-bg-img" style="background-image: url(<?= $img_main_bg ?>)">
            <div class="col text-center text-white">
                <?= $txt_main_title1 ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 dp-bg-img" style="background-image: url(<?= $img_left_bg ?>)"></div>
            <div class="col-12 col-md-6 dp-bg-primary text-white d-flex align-items-center">
                <div class="wrapper h-75 w-75 mx-auto d-flex flex-column justify-content-around">
                    <div class="mision">
                        <h2 class="text-uppercase">
                            <?= $txt_mision_title ?>
                        </h2>
                        <?= $txt_mision_p ?>
                    </div>
                    <div class="vision">
                        <h2 class="text-uppercase">
                            <?= $txt_vision_title ?>
                        </h2>
                        <?= $txt_vision_p ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>