<?php 

/* Template Name: Home Template */ 

?>

<section class="container-fluid content-principal" style="">
        <img src="wp-content/themes/copergo/assets/images/portada.jpg" alt="">
</section>

<section class="container-fluid content-bloques">
    <article class="row">
        <article class="col-xl-4 col-lg-4">
            <article class="row bloque">
                <article class="col-xl-8 col-lg-8 p-0 ">
                    <img src="wp-content/themes/copergo/assets/images/copergo-1.jpg" alt="">
                </article>
                <article class="col-xl-4 col-lg-4 p-0">
                    <article class="text-bloque">
                        <article class="title">
                            <h2>NOSOTROS</h2>
                        </article>
                        <article class="ver_mas">
                            <a href="#">VER MÁS</a>
                        </article>
                    </article>                    
                </article>
            </article>
        </article>
        <article class="col-xl-4 col-lg-4">
            <article class="row bloque">
                <article class="col-xl-8 col-lg-8 p-0 h-100">
                    <img src="wp-content/themes/copergo/assets/images/copergo-2.jpg" alt="">
                </article>
                <article class="col-xl-4 col-lg-4 p-0">
                    <article class="text-bloque">
                        <article class="title">
                            <h2>OBRAS</h2>
                        </article>
                        <article class="ver_mas">
                            <a href="#">VER MÁS</a>
                        </article>
                    </article>                    
                </article>
            </article>
        </article>
        <article class="col-xl-4 col-lg-4">
            <article class="row bloque">
                <article class="col-xl-8 col-lg-8 p-0 h-100">
                    <img src="wp-content/themes/copergo/assets/images/copergo-3.jpg" alt="">
                </article>
                <article class="col-xl-4 col-lg-4 p-0">
                    <article class="text-bloque">
                        <article class="title">
                            <h2>Contacto</h2>
                        </article>
                        <article class="ver_mas">
                            <a href="#">VER MÁS</a>
                        </article>
                    </article>                    
                </article>
            </article>
        </article>
    </article>
</section>