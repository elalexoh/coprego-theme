<footer class="content-info container-fluid">
  <!-- Comentado por Ender Bastidas para eliminar Archivos, Categorías, Meta.
  <div class="container">
  <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
-->
  <section class="container">
    <article class="row">

      <article class="col-xl-3 col-lg-3">
        <article>
          <h1>COPERGO</h1>
          <p>Desde 1980</p>
          <p>Copergo©. All Rights Reserved. 2020</p>
        </article>
      </article>

      <article class="col-xl-3 col-lg-3">
        <article>
          <p>Santiago/Chile</p>
          <p>Pdte Eduardo Frei Montalva 3348, Renca, Santiago, Chile</p>
        </article>
      </article>
     
      <article class="col-xl-3 col-lg-3">
        <article>
          <p>Contacto Comercial</p>
          <p>¿Tienes alguna duda comercial?</p>
          <p>contacto@copergo.com</p>
          <p>+56-2-224064900</p>
        </article>
      </article>

      <article class="col-xl-3 col-lg-3">
        <article>
          <p>NEWSLETTER</p>
        </article>
      </article>

    </article>
  </section>
</footer>
